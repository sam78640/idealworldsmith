# -*- coding: utf-8 -*-
import scrapy
import time
from bs4 import BeautifulSoup


class QessaysSpider(scrapy.Spider):
    name = 'qessays'
    # allowed_domains = ['http://blog.qessays.com']
    page = 1
    start_urls = ['http://blog.qessays.com/page/2']



    def parse(self, response):
        next_page_url = response.xpath('//a[@class="next page-numbers"]/@href').extract()[0]
        read_more_urls = []
        for item in response.xpath('//a[@class="read-more-button"]/@href').extract():
            yield scrapy.Request(url=item,callback=self.parseBlog)
        print(next_page_url)
        yield scrapy.Request(url=next_page_url, callback=self.parse)

    
    def parseBlog(self, response):
        body = response.body
        soup = BeautifulSoup(body, 'html.parser')
        title = soup.find('h1',{'class': 'single-title'}).text
        content = soup.find('div',{'class': 'entry-content-inner'}).text
        yield {'title': title, 'content': content}